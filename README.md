# offline-radio

Using tcp/ip for real time data transmission have obvious pitfalls.
Even if data is a radio stream.
So let's record radio and play next day but same time.
For example in car, subway or other internetless locations and conditions.

**The current design** (can be changed without notice)
- site: shows existing recordings
- recorder: opens the webpage, click play and grab audio stream
- sync: syncs recorded tracks against mobile or SD card
- transcoder: transcodes recorded wav (PCM) into mp3

**Requirements:**
- Linux (tested on Ubuntu 18+)
- Chromium (installed via snap)
- ffmpeg

