package bandeschran.offlineradio.domain;

import java.time.LocalDateTime;

public class Track {

    private LocalDateTime start;
    private LocalDateTime end;
    private byte[] data;

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
