package bandeschran.offlineradio.service;

import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.util.Arrays.asList;

public class TrackService {

    public static final String TRACK_PREFIX = "radio-";
    public static final String TRACK_SUFFIX = ".wav";

    @Value("${application.working.path}")
    private String workingPath;

    public static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
            .append(ISO_LOCAL_DATE)
            .appendLiteral('_')
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2).toFormatter();

    public String getDestinationFile() {
        final String dateTime = LocalDateTime.now().format(DATE_TIME_FORMATTER);
        return workingPath + "/" + TRACK_PREFIX + dateTime + TRACK_SUFFIX;
    }

    public LocalDateTime parseFileName(String fileName) {
        String dateTime = fileName
            .substring(0, fileName.lastIndexOf("."))
            .replace(TRACK_PREFIX, "");
        return LocalDateTime.parse(dateTime, DATE_TIME_FORMATTER);
    }

    public List<String> getFolderContent() {
        final Path path = Paths.get(workingPath);
        return getPathContent(path);
    }

    public List<String> getFolderContent(String folder) {
        final Path path = Paths.get(workingPath, folder);
        return getPathContent(path);
    }

    public String getWorkingPath() {
        return workingPath;
    }

    public void validateWorkingPath() {
        File folderToValidate = Paths.get(workingPath).toFile();
        if (folderToValidate.isFile()) {
            throw new RuntimeException("Unexpectedly instead of destination folder found file");
        }

        if (!folderToValidate.exists() || !folderToValidate.isDirectory()) {
            throw new RuntimeException("Destination folder doesn't exist, please reconfigure or create one");
        }
    }

    private List<String> getPathContent(Path path) {
        try (Stream<Path> list = Files.list(path)) {
            return list.map(file -> file.getFileName().toString())
                    .sorted().collect(Collectors.toList());
        } catch (IOException e) {
            return asList("Path " + path + " is not valid");
        }
    }

}
