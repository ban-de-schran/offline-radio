package bandeschran.offlineradio;

import bandeschran.offlineradio.service.TrackService;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class AudioGrabber {

    @Value("${application.recording.command}")
    private String recordingCommand;

    @Value("${application.recording.onlineRadioUrl}")
    private String onlineRadioUrl;

    @Value("${application.recording.trackDurationSecond}")
    private Integer trackDurationSecond;

    @Autowired
    private TrackService trackService;

    @Autowired
    private WebDriver webDriver;

    private static final String PLAY_BUTTON_ID = "webradioPlay";
    private static final Logger LOG = LoggerFactory.getLogger(AudioGrabber.class);

    public void record() {

        trackService.validateWorkingPath();

        webDriver.get(onlineRadioUrl);
        // webDriver.findElement(By.id(PLAY_BUTTON_ID)).click();
        webDriver.findElement(By.id(PLAY_BUTTON_ID)).sendKeys(Keys.ENTER);

        while(true) {

            try {
                String fileName = trackService.getDestinationFile();
                final Process process = Runtime.getRuntime().exec(recordingCommand + " " + fileName);
                LOG.info("Run recording for " + trackDurationSecond + " sec into file " + fileName);
                Thread.sleep(trackDurationSecond * 1000);
                process.destroy();
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }

    }

}
