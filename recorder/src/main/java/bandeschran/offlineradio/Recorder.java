package bandeschran.offlineradio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(RecorderConfig.class)
public class Recorder implements CommandLineRunner {

	@Autowired
	private AudioGrabber audioGrabber;

	private static final Logger LOG = LoggerFactory.getLogger(Recorder.class);

	public static void main(String[] args) {
		LOG.info("Starting recording");
		SpringApplication.run(Recorder.class, args);
		LOG.info("Finish recording");
	}

	@Override
	public void run(String... args) {
		LOG.info("Do the recording");

		audioGrabber.record();

	}
}
