package bandeschran.offlineradio;

import bandeschran.offlineradio.service.TrackService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class RecorderConfig {

    @Value("${webdriver.chrome.driver}")
    private String pathToWebdriver;

    @Bean
    public AudioGrabber audioGrabber() {
        return new AudioGrabber();
    }

    @Bean
    public TrackService trackService() {
        return new TrackService();
    }

    private static final Logger LOG = LoggerFactory.getLogger(RecorderConfig.class);

    @Bean
    public WebDriver webDriver() {

        final ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--no-sandbox");
        // Known issue https://github.com/SeleniumHQ/selenium/issues/7788 when ubuntu + snap + chromium

        try {
            return new ChromeDriver(
                    (new ChromeDriverService.Builder() {
                        @Override
                        protected File findDefaultExecutable() {
                            return new File(pathToWebdriver) {
                                @Override
                                public String getCanonicalPath() {
                                    return this.getAbsolutePath();
                                }
                            };
                        }
                    }).build(), chromeOptions);
        } catch (Exception ex) {
            LOG.error("Unable to instantiate Chrome web driver");
            throw ex;
        }
    }

}
