package bandeschran.offlineradio.ui;

import bandeschran.offlineradio.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class Index {

    @Autowired
    private TrackService trackService;

    @GetMapping({"/", ""})
    public String root(Model model) {
        model.addAttribute("folders", trackService.getFolderContent());
        return "index.html";
    }

    @GetMapping({"/folder/{folder}"})
    public String showFolder(Model model, @PathVariable String folder) {
        model.addAttribute("folders", trackService.getFolderContent());
        model.addAttribute("selectedFolder", folder);
        model.addAttribute("folderContent", trackService.getFolderContent(folder));
        return "index.html";
    }

}
