package bandeschran.offlineradio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SiteConfig.class)
public class Site {

	public static void main(String[] args) {
		SpringApplication.run(Site.class, args);
	}

}
