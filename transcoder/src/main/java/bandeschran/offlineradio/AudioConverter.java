package bandeschran.offlineradio;

import bandeschran.offlineradio.service.TrackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.stream.Stream;

public class AudioConverter {

    @Value("${application.transcoding.command}")
    private String transcodingCommand;

    @Value("${application.working.path}")
    private String workingPath;

    @Autowired
    private TrackService trackService;

    private static final String OUTGOING_FOLDER = "/mp3/";

    private static final Logger LOG = LoggerFactory.getLogger(AudioConverter.class);

    public void transcode() {

        try (Stream<Path> list = Files.list(Paths.get(workingPath))) {
            list
                .filter(file -> !file.toFile().isDirectory())
                .filter(file -> !file.endsWith(".wav"))

                // The latest one probably is still recording
                .sorted((pathB, pathA) -> byCreated(pathA, pathB))
                .skip(1L)
                //
                .sorted(this::byCreated)
                .forEach(this::process);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    private int byCreated(Path pathA, Path pathB) {
        LocalDateTime localDateTimeA = trackService.parseFileName(pathA.getFileName().toString());
        LocalDateTime localDateTimeB = trackService.parseFileName(pathB.getFileName().toString());

        return localDateTimeA.compareTo(localDateTimeB);
    }


    private void process(Path path) {
        try {
            String command = String.format(transcodingCommand, path.toString());
            command += " " + workingPath + OUTGOING_FOLDER + path.getFileName().toString().replace(".wav", ".mp3");
            final Process process = Runtime.getRuntime().exec(command);
            process.waitFor();
            path.toFile().delete();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}
