package bandeschran.offlineradio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(TranscoderConfig.class)
public class Transcoder implements CommandLineRunner {

	@Autowired
	private AudioConverter audioConverter;

	private static Logger LOG = LoggerFactory.getLogger(Transcoder.class);

	public static void main(String[] args) {
		LOG.info("Starting transcoding");
		SpringApplication.run(Transcoder.class, args);
		LOG.info("Finish transcoding");
	}

	@Override
	public void run(String... args) {
		LOG.info("Executing transcoding");
		audioConverter.transcode();
	}
}
