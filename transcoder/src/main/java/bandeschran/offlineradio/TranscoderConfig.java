package bandeschran.offlineradio;

import bandeschran.offlineradio.service.TrackService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TranscoderConfig {

    @Bean
    public AudioConverter audioConverter() {
        return new AudioConverter();
    }

    @Bean
    public TrackService trackService() {
        return new TrackService();
    }

}
