package bandeschran.offlineradio;

import bandeschran.offlineradio.service.TrackService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SynchronizerConfig {

  @Bean
  public FileSystemSynchronizer fileSystemSynchronizer() {
    return new FileSystemSynchronizer();
  }

  @Bean
  public TrackService trackService() {
    return new TrackService();
  }


}
