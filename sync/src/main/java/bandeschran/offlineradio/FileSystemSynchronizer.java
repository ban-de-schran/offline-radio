package bandeschran.offlineradio;

import bandeschran.offlineradio.service.TrackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class FileSystemSynchronizer {

/*
  @Value("${application.working.path}")
  private String pathFromConfig;

  @Value("${application.working.destination}")
  private String pathToConfig;
*/

  @Value("${application.expiring.days}")
  private Integer expiringDays;

  @Autowired
  private TrackService trackService;

  private static final Logger LOG = LoggerFactory.getLogger(FileSystemSynchronizer.class);

/*
  private final Path pathFrom = Paths.get(pathFromConfig);
  private final Path pathTo = Paths.get(pathToConfig);
*/

  @Value("${application.working.path}")
  private Path pathFrom;

  @Value("${application.working.destination}")
  private Path pathTo;


  public void synchronize() {

    validatePaths();

    try {
      Files.list(pathFrom)
          .forEach(this::doSynchronization);
    } catch (IOException ex) {
      LOG.error(ex.getMessage());
    }
  }

  private void doSynchronization(Path currentFile) {
    LOG.debug("Processing file " + currentFile);
    String fileName = currentFile.getFileName().toString();
    Path newFile = Path.of(pathTo + "/" + fileName);
    if (!newFile.toFile().exists()) {
      try {
        LocalDateTime fileDateTime = trackService.parseFileName(fileName);
        if (fileDateTime.isBefore(LocalDateTime.now().minusDays(expiringDays))) {
          LOG.debug("File will be moved to " + newFile);
          Files.move(currentFile, newFile);
        } else {
          LOG.debug("File will be copied to " + newFile);
          Files.copy(currentFile, newFile);
        }
      } catch (IOException ex) {
        LOG.error(ex.getMessage());
      }
    }
  }

  private void validatePaths() {
    if (!pathFrom.toFile().exists()) {
      throw new RuntimeException("From folder doesn't exist, exiting");
    }

    if (!pathTo.toFile().exists()) {
      throw new RuntimeException("To folder doesn't exist, exiting");
    }
  }

}
