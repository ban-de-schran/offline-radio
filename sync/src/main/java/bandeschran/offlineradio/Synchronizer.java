package bandeschran.offlineradio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SynchronizerConfig.class)
public class Synchronizer implements CommandLineRunner {

	@Autowired
	private FileSystemSynchronizer fileSystemSynchronizer;

	private static Logger LOG = LoggerFactory.getLogger(Synchronizer.class);

	public static void main(String[] args) {
		LOG.info("Starting recordings synchronization");
		SpringApplication.run(Synchronizer.class, args);
		LOG.info("Finish recordings synchronization");
	}

	@Override
	public void run(String... args) {
		LOG.info("Executing synchronization");
		fileSystemSynchronizer.synchronize();
	}
}
